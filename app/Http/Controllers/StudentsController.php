<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Models\Siswa;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
   
    {
       
        $siswa = siswa::all();
        
        return view('student', ['siswa' => $siswa]);
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nama'=>'required',
            'kelas'=>'required',
            'jurusan'=>'required',
            'photo'=>'required'

        ]);

        Siswa::create($request->all());
        // Siswa::create([
        //     'nama' => $request->nama,
        //     'kelas' => $request->kelas,
        //     'jurusan' => $request->jurusan,
        //     'photo' => $request->photo,

        // ]);

        return redirect('student');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($siswa)
    {
        //
        return view('edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return show ('edit',[$siswa->id]);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Siswa $siswa ,$id)
     {

        $deletedRows = $siswa::where('id', $id)->delete();
      
    // Siswa::destroy($siswa->id);
    return redirect('/student')-> with('status','berhasil di hapus!');
    // DB::table('siswa')->delete();

    //     DB::table('siswa')->where('votes', '>', 1)->delete();

        // $siswa::withTrashed()
        //         ->where('account_id', 16)
        //         ->get();
    }
}

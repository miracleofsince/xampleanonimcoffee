@extends('layout/main')

@section('title','web dinamis')



@section('container')
<p>
<div class="container">
<div class="ROW">


<div class="col-md-5" >
<form action="/student" method="post" >  
@csrf
<label for="">input data siswa</label>
  <div class="form-group">
 
    <label for="formGroupExampleInput">cerita :</label>
    <textarea class="form-control col-4 @error ('nama') is-invalid @enderror texteditor " id="formGroupExampleInput" placeholder="Masukan nama" name="nama" value="{{ old('nama') }}"> 
    @error('nama')
    <div class="invalid-feedback">{{ $message }}</div>@enderror
    </textarea>
  </div>
  
  <div class="form-group">
    <label for="formGroupExampleInput">Kelas :</label>
    <input type="text" class="form-control col-4" id="formGroupExampleInput" placeholder="masukan kelas" name="kelas"> 
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Jurusan :</label>
    <input type="text" class="form-control col-4" id="formGroupExampleInput2" placeholder="masukan kelas" name="jurusan">

   
  <div class="form-group">
    <label for="exampleFormControlFile1">Example file input</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="photo">
    <button type="submit" class="btn btn-primary mt-2">simpan</button>

</div>
</form>
</div>

</div>
<div class="col-md-7">

@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif

<div class="table">
<table>
<tr>
<thead class="thead-dark">

<th>no</th>
<th>nama</th>
<th>kelas</th>
<th>jurusan</th>
<th>photo</th>
<th>aksi</th>
</thead>

</tr>
@foreach($siswa as $siswa )

<tr>

<td>{{ $siswa->id }}</td>
<td>{{ $siswa->nama }}</td>
<td>{{ $siswa->kelas }}</td>
<td>{{ $siswa->jurusan }}</td>
<td>{{ $siswa->photo }}</td>
<td>


<a href="{{ url('student/'.$siswa->id) }}" class="badge badge-success" style="width:50px;" name="edit" id="edit">edite</a>


<form action="{{ url('student/'.$siswa->id) }}" method="post">
@method('delete')
@csrf
<button class="badge badge-danger" type="submit" style="width:50px;">hapus</button>
</form>
</td>

</tr>
@endforeach
</table>
</div>



</div>
</div>
</div>

@endsection

</div>

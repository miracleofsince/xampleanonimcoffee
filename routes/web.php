<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });
Route::get('/','indexsController@index');

// Route::get('/student', function () {
//     return view('student');
// });
// Route::get('/student', 'StudentsController@index');
// Route::post('/student', 'StudentsController@store');
// Route::delete('/student','StudentsController@destroy');
Route::resource('/student','StudentsController');
Route::get('/student.$siswa','StudentsController@edit');


Route::resource('/index','indexsController');
